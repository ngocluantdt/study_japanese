<?php
header("Access-Control-Allow-Origin: *");
require('Tool.php');
$config['secretary'] = [
    'spreadsheetID'=>'1mvHwo3xCRwwzFG0GRVjzIxo-zyZ6NPSd6A3Brycf06M',
    'per_page'=>'50',
    'link_redmine'=>'https://mid-dev-redmine.fraise.jp/projects/midream/issues?query_id=88',
    'sheetName'=>'Data',
    'sheet_id'=>'603681342',
    'credentials'=>'credentials.json',
    'token'=>'token.json',
];

$tool = new Tool($config['secretary']);

header('Content-Type:application/json');
if($tool->readSheet()){
   echo json_encode($tool->getData());
}
exit();