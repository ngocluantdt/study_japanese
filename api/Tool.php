<?php
/**
 * Created by PhpStorm.
 * User: quoc_vuong
 * Date: 10/31/2018
 * Time: 1:55 PM
 */
date_default_timezone_set('Asia/Ho_Chi_Minh');


require 'vendor/autoload.php';
//require 'common/vendor/simplehtmldom_1_5/simple_html_dom.php';


class Tool
{
    private $googleClient;
    private $dataSheet;
    private $dataRedmine;
    private $googleService;
    private $config;
    public $error;
    public $qty_rows_update;
    public $qty_rows_append;
    private $run_time;
    private $ticket_id_update = [];
    private $ticket_id_add = [];
    private $CI;

    public function __construct($config)
    {

        $this->config = $config;

        $this->getClient();
        global $run_time;
        $this->run_time = $run_time;
        $this->googleService = new Google_Service_Sheets($this->googleClient);

    }

    protected function getClient()
    {
        $this->googleClient = new Google_Client();
        $this->googleClient->setApplicationName('Google Sheets API PHP Quickstart');
        $this->googleClient->setScopes(Google_Service_Sheets::SPREADSHEETS);
        $this->googleClient->setAuthConfig($this->config['credentials']);
        $this->googleClient->setAccessType('offline');
        $this->googleClient->setPrompt('select_account consent');

        // Load previously authorized token from a file, if it exists.
        $tokenPath = $this->config['token'];
        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $this->googleClient->setAccessToken($accessToken);
        }

        // If there is no previous token or it's expired.
        if ($this->googleClient->isAccessTokenExpired()) {
            // Refresh the token if possible, else fetch a new one.
            if ($this->googleClient->getRefreshToken()) {
                $this->googleClient->fetchAccessTokenWithRefreshToken($this->googleClient->getRefreshToken());
            } else {
                // Request authorization from the user.
                $authUrl = $this->googleClient->createAuthUrl();
                printf("Open the following link in your browser:\n%s\n", $authUrl);
                print 'Enter verification code: ';
                $authCode = trim(fgets(STDIN));

                // Exchange authorization code for an access token.
                $accessToken = $this->googleClient->fetchAccessTokenWithAuthCode($authCode);
                $this->googleClient->setAccessToken($accessToken);

                // Check to see if there was an error.
                if (array_key_exists('error', $accessToken)) {
                    throw new Exception(join(', ', $accessToken));
                }
            }
            // Save the token to a file.
            if (!file_exists(dirname($tokenPath))) {
                mkdir(dirname($tokenPath), 0700, true);
            }
            file_put_contents($tokenPath, json_encode($this->googleClient->getAccessToken()));
        }
    }

    public function readSheet()
    {
        try{
            $spreadsheetName = 'Basic';
            $spreadsheetId = $this->config['spreadsheetID'];
            $sheetInfo = $this->googleService->spreadsheets_values->get($spreadsheetId,$spreadsheetName);

            $this->dataSheet['basic'] = $sheetInfo->getValues();
            array_shift($this->dataSheet['basic']);

            $spreadsheetName = 'Am_ghep';
            $spreadsheetId = $this->config['spreadsheetID'];
            $sheetInfo = $this->googleService->spreadsheets_values->get($spreadsheetId,$spreadsheetName);
            $this->dataSheet['am_ghep'] = $sheetInfo->getValues();
            array_shift($this->dataSheet['am_ghep']);

            $spreadsheetName = 'Kanji';
            $spreadsheetId = $this->config['spreadsheetID'];
            $sheetInfo = $this->googleService->spreadsheets_values->get($spreadsheetId,$spreadsheetName);
            $this->dataSheet['kanji'] = $sheetInfo->getValues();
            array_shift($this->dataSheet['kanji']);

            return true;
        }
        catch (Exception $e){
            return false;
        }

    }

    public function getData(){
        foreach ($this->dataSheet as $key=>$item){
            if($key == 'kanji'){
                $item = array_map(function($val){
                    return [
                        'k'=>isset($val[0])?$val[0]:'',
                        'o'=>isset($val[1])?$val[1]:'',
                        'ku'=>isset($val[2])?$val[2]:'',
                        'h'=>isset($val[3])?$val[3]:'',
                        'v'=>isset($val[4])?$val[4]:'',
                    ];
                },$item);
            }
            else{
                $item = array_map(function($val){
                    return [
                        'r'=>$val[0],
                        'h'=>$val[1],
                        'k'=>$val[2]
                    ];
                },$item);
            }
            $this->dataSheet['version'] = 1.2;
            $this->dataSheet[$key] = $item;
        }
        //readed sheet Basic
//        $romanji =  array_column($this->dataSheet['basic'],0);
//        $katakana =  array_column($this->dataSheet['basic'],1);
//        $hiragana =  array_column($this->dataSheet['basic'],2);
//        $katakana_map = [];
//        $hiragana_map = [];
//        foreach($romanji as $key=>$item){
//            $katakana_map[]= [$item,$katakana[$key],'k'];
//            $hiragana_map[] = [$item,$hiragana[$key],'h'];
//        }
        return $this->dataSheet;
    }

//
//    //delete data sheet
    private function clearSheet()
    {
        $requestBody = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
            'requests' => [
                new Google_Service_Sheets_Request([
                    'deleteDimension' => [
                        'range' => [
                            'sheetId' => $this->config['sheet_id'],
                            'dimension' => 'ROWS',
                            'startIndex' => 1,
                            'endIndex' => 100
                        ]
                    ]
                ]),
            ]
        ]);

        if (!$this->googleService->spreadsheets->batchUpdate($this->config['spreadsheetID'], $requestBody)) {
            $this->error[] = 'Clear sheet web occur error';
        }
        return true;
    }

    public function writeSheet($data){

//        $this->clearSheet();
        if(empty($data)){
            return false;
        }

        foreach ($data as $row) {

            $body = new Google_Service_Sheets_ValueRange([
                'values' => [array_values($row)]
            ]);


            $range = $this->config['sheetName'] . '!A1';
            $result = $this->googleService->spreadsheets_values->append($this->config['spreadsheetID'], $range,
                $body, [
                    'valueInputOption' => 'RAW'
                ]);

        }
        return true;
    }
    /* private*/
    public function run()
    {
        $range = $this->config['sheetName'];
        $spreadsheetId = $this->config['spreadsheetID'];

        if (!$this->readSheet() || !$this->readRedmine() || !$this->clearSheet() || !$this->readSheet()) {
            return false;
        }

        if (!$this->dataRedmine) {
            return true;
        }
        $this->qty_rows_update = 0;
        $this->qty_rows_append = 0;

        $ids_jp_on_redmine = array_column($this->dataRedmine, 'jp_id');
        $intersect_arr = [];
        if ($this->dataSheet) {
            $ids_jp_on_sheet = array_column($this->dataSheet, 0);
            $intersect_arr = array_intersect($ids_jp_on_sheet, $ids_jp_on_redmine);
            $this->qty_rows_update = count($intersect_arr);
        }


        foreach ($this->dataRedmine as $row) {
            if (empty($row['jp_id'])) {
                continue;
            }
            $parent_id = $row['jp_parent_id'] ? '(' . $row['jp_parent_id'] . ')' : '';
            $update_date = [
                $row['jp_id'],
                "=HYPERLINK(\"" . $row['jp_url'] . "\",\"" . $row['jp_id'] . $parent_id . "\")",
                "=HYPERLINK(\"" . $row['vn_url'] . "\",\"" . $row['vn_id'] . "\")",
                $row['jp_status'],
                $row['jp_priority'],
                $row['jp_assign'],
                $row['vn_title'],
                $row['vn_assign'],
                $row['vn_status'],
                $row['vn_start_date'],
                $row['vn_due_date'],
                $row['vn_estimated_time'],
                $row['vn_spent_time'],
                $row['vn_progress'],
                $row['jp_branch'],
            ];
            $body = new Google_Service_Sheets_ValueRange([
                'values' => [$update_date]
            ]);


            if ($intersect_arr && ($row_update_on_sheet = array_search($row['jp_id'], $intersect_arr)) !== false) {
                $range = $this->config['sheetName'] . '!A' . ($row_update_on_sheet + 1);
                $result = $this->googleService->spreadsheets_values->update($spreadsheetId, $range,
                    $body, [
                        'valueInputOption' => 'USER_ENTERED'
                    ]
                );
                $this->ticket_id_update[] = $row['jp_id'];
                continue;
            }
            $range = $this->config['sheetName'] . '!A1';
            $result = $this->googleService->spreadsheets_values->append($spreadsheetId, $range,
                $body, [
                    'valueInputOption' => 'USER_ENTERED'
                ]);
            $this->qty_rows_append++;
            $this->ticket_id_add[] = $row['jp_id'];

        }
        //write sheet
        $body = new Google_Service_Sheets_ValueRange([
            'values' => [
                ['Last Updated : ' . $this->run_time]
            ]
        ]);


        $range = $this->config['sheetName'] . '!D1';
        $result = $this->googleService->spreadsheets_values->update($spreadsheetId, $range,
            $body, [
                'valueInputOption' => 'RAW'
            ]
        );

        $this->writeLog();
        return true;

    }

    private function writeLog()
    {
//write file
        global $file_name;

        $myfile = fopen($file_name, "r") or die("Unable to open file!");
        $txt = fread($myfile, filesize($file_name));
        fclose($myfile);
        $txt .= "\nEnd time : " . date('Y-m-d H:i:s');
        $txt .= "\n-----------------------------";
        $txt .= "\n Tickets updated";
        $txt .= "\n" . var_export($this->ticket_id_update, true);
        $txt .= "\n Tickets added";
        $txt .= "\n" . var_export($this->ticket_id_add, true);
        $myfile = fopen($file_name, "w") or die("Unable to open file!");
        fwrite($myfile, $txt);
        fclose($myfile);
    }

    public function getError()
    {
        return count($this->error) ? $this->error : '';
    }

    public function getUpdateRows()
    {
        return $this->ticket_id_update;
    }

    public function getAppendRows()
    {
        return $this->ticket_id_add;
    }
}