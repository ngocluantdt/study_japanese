import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/page/Home'
import Basic from '@/page/Basic'
import Practice from '@/page/Practice'
import PracticeJP from '@/page/PracticeJP'

import Kanji from '@/page/Kanji'
import KanjiFindRomanji from '@/page/KanjiFindRomanji'
import KanjiFindKanji from '@/page/KanjiFindKanji'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Home',
            component: Home,
        },
        {
            path: '/basic',
            name: 'Basic',
            component: Basic,
        },
        {
            path: '/practice',
            name: 'Practice',
            component: Practice,
        },
        {
            path: '/practicejp',
            name: 'PracticeJP',
            component: PracticeJP,
        },
        {
            path: '/kanji',
            name: 'Kanji',
            component: Kanji,
        },
        {
            path: '/kanjifindromanji',
            name: 'KanjiFindRomanji',
            component: KanjiFindRomanji,
        },
        {
            path: '/kanjifindkanji',
            name: 'KanjiFindKanji',
            component: KanjiFindKanji,
        }
    ]
})
