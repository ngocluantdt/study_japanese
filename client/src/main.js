// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
var abc= 'luan'
import Vue from 'vue'
import App from './App'
import Vuex from 'vuex'
import router from './router'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.css';
import Helper from './helper.js'
import storeOption from './store.js'

import swal from 'sweetalert';



Vue.config.productionTip = false
Vue.config.devtools = false
Vue.use(Helper)
Vue.use(Loading,{
    canCancel: true, // default false
    // onCancel: this.yourMethodName,
    color: 'blue',
    width: 64,
    height: 64,
    backgroundColor: 'gray',
    opacity: 0.5,
    zIndex: 999,
    loader:'spinner'  // spinner or dots or bars
});




Vue.use(Vuex)
const store = new Vuex.Store(storeOption)

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    components: {App},
    template: '<App/>',
    store,
    data:{
        // apiUrl:'http://localhost/study_japanese/api/',
        apiUrl:'http://study-japanese.vinaweber.com/api/',
        mainData :null,
        practiceChar : [],
        practiceType: 0,
        practiceRange:[]
    },

})
