
export default {
    state: {
        $vue:{},
        practicelist: {
            data:[],
            tracker:0
        },
        randomData:{
            data:[],

        },
        resultData:{
            data:{
                chars:[],
                userAnswerText:'',
                userAnswerStatus:''
            },
            tracker:0
        },

        statistic:{
            incorrect:0,
            correct:0,
            total:0
        },

        wrongDataBasic:{
            data:[],
            tracker:0
        },
        wrongDataKanji:{
            data:[],
            tracker:0
        }
    },
    getters: {
        practicelist: state => {
            state.practicelist.tracker
            return  state.practicelist.data
        },
        randomData: state => {
            return  state.randomData.data
        },
        resultData: state => {
            state.resultData.tracker
            return state.resultData.data
        },
        wrongDataBasic: state => {
            state.wrongDataBasic.tracker
            return state.wrongDataBasic.data
        },
        statistic: state => {
            return state.statistic
        },

    },
    mutations: {
        updatePracticelist (state) {
            var bg_class = 'bg-danger'
            if(state.resultData.data.userAnswerStatus == true){
                var bg_class = 'bg-success'
                state.statistic.correct++
            }
            var keyChar = 'r'
            if(state.$vue.$root.practiceType ==1){ // for kanji
                 keyChar = 'k'
            }
            //console.log('keyChar',keyChar)

            state.practicelist.data.map(function(item){
                if(item[keyChar] == state.resultData.data.chars[keyChar] && item.class ==''){
                    return item['class'] = bg_class
                }

            })
            this.commit('updateStatistic')
            state.practicelist.tracker++
        },
        updateStatistic(state){
            var countInCorrrect = 0;
            state.practicelist.data.map(function(item){
                if(item.class == 'bg-danger'){
                    countInCorrrect++
                }
                return item;
            })
            state.statistic.incorrect = countInCorrrect
            state.statistic.total = state.practicelist.data.length
        },
        createPracticeList(state,data){
            state.practicelist.data= data
            this.commit('updateStatistic')
            state.practicelist.tracker++
        },
        updateRandomData(state,data){
            state.randomData.data =data
            //console.log(' state.randomData.data', state.randomData.data)

        },
        updateResultData(state,data){
            state.resultData.data = data
            state.resultData.tracker++
            this.commit('updatePracticelist')
            this.commit('updateWrongDataBasic')
             //console.log('resultdata',data)

        },

        resetForm(state){
            // state.resultData.tracker++

            state.resultData.data.chars=[]
            state.resultData.data.userAnswerText = ''
            state.resultData.data.userAnswerStatus = ''

            state.practicelist.data.map(function(item){
                item.class=''
                return item
            })

            state.statistic.correct = 0;
            state.statistic.incorrect = 0;

            state.resultData.tracker++
            state.practicelist.tracker++

        },
        loadWrongDataBasic(state){
            if(state.wrongDataBasic.data.length == 0){
                state.wrongDataBasic.data = state.$vue.getLocalStorage('wrongDataBasic')
                if(state.wrongDataBasic.data == null){
                    state.wrongDataBasic.data = []
                }
            }
            state.wrongDataBasic.tracker++
        },
        updateWrongDataBasic(state){
            state.wrongDataBasic.tracker++
            var resultData = state.resultData.data
            var key = null;
            if(state.wrongDataBasic.data.length){
                state.wrongDataBasic.data.map(function(item,k){
                    if(item.r == resultData.chars.r){
                        key = k
                    }
                    return item
                })
            }
            if(resultData.userAnswerStatus){
                if(key != null){
                    if(state.wrongDataBasic.data[key]['wrongNumber'] <= 1){
                        // console.log('deteck')
                        state.wrongDataBasic.data.splice(key,1)
                    }
                    else{
                        state.wrongDataBasic.data[key]['wrongNumber']-=1
                    }
                }
            }
            else{
                if(key != null){
                    state.wrongDataBasic.data[key]['wrongNumber'] += 1
                }
                else{
                    var addData = state.$vue.cloneObject(resultData.chars)
                    addData['wrongNumber'] = 1
                    state.wrongDataBasic.data.push(addData)
                }
            }
           // console.log('state.wrongDataBasic.data',state.wrongDataBasic.data)
            state.$vue.setLocalStorage('wrongDataBasic',state.wrongDataBasic.data)
        },
        sendInstance(state,instance){
            state.$vue = instance
        }
    }
}