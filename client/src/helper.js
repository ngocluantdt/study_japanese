export default {
    install(Vue,option){
        Vue.mixin({
            methods: {
                cloneObject:function(obj){
                    if (typeof obj == "object") {
                        var cloneObj = Array.isArray(obj) ? [] : {};
                        for (var k in obj) {
                            cloneObj[k] = this.cloneObject(obj[k]);
                        }
                        return cloneObj;
                    }
                    return obj;
                },
                setLocalStorage:function(key,data){

                    if (typeof(Storage) !== "undefined") {
                        if (typeof data == "object") {
                            console.log('this is object')
                            data = JSON.stringify(data)
                        }
                        localStorage.setItem(key, data);
                    } else {
                        // Không hỗ trợ local storage
                        console.log('Trình duyệt không hỗ trợ local storage')

                    }
                },
                getLocalStorage:function(key){
                    if (typeof(Storage) !== "undefined") {
                        var data = localStorage.getItem(key);
                        try {
                            return JSON.parse(data);
                        } catch (e) {
                            return data;
                        }
                    } else {
                        // Không hỗ trợ local storage
                        console.log('Trình duyệt không hỗ trợ local storage')

                    }
                },
                setCookie:function(name,value,days) {
                    var expires = "";
                    if (days) {
                        var date = new Date();
                        date.setTime(date.getTime() + (days*24*60*60*1000));
                        expires = "; expires=" + date.toUTCString();
                    }
                    document.cookie = name + "=" + (value || "")  + expires + "; path=/";
                },
                getCookie:function(name) {
                    var nameEQ = name + "=";
                    var ca = document.cookie.split(';');
                    for(var i=0;i < ca.length;i++) {
                        var c = ca[i];
                        while (c.charAt(0)==' ') c = c.substring(1,c.length);
                        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
                    }
                    return null;
                },
                random(min,max){
                    return Math.floor(Math.random() * (max-min+1)+min)
                },
                debug:function(name,message){
                    console.log(name+" : ",message)
                }
            }
        })
    }
}